1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування.
   Щоб використати спеціальний символ як звичайний, потрібно додати до нього зворотну косу межу: . - Це називається «екранування символу».
2. Які засоби оголошення функцій ви знаєте?
   "function declaration statement"
   function square(number) {
      return number * number;
   }
   function myFunc(theObject) {
   theObject.make = 'Toyota';
   }
   "function definition expression"
   var square = function(number) { return number * number; };
3. Що таке hoisting, як він працює для змінних та функцій?
   hoisting — це механізм JavaScript, в якому змінні та оголошення функцій, пересуваються вгору своєї області видимості перед тим, як код буде виконано.
